# OpenML dataset: Reliance-Industries-(RIL)-Share-Price-(1996-2020)

https://www.openml.org/d/43816

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
We have daily stock prices of Reliance Industries (RIL) the parent of telecom company Jio Platform for which we have multiple investments in India including from Facebook and Intel
This is owned by Mukesh Ambani, the richest person in India. Also, among the top 10 richest person in the world! 
Data Dictionary
Date    ==== Date of information
Symbol    ==== Name of share. Reliance Industries in this case
Series    ==== Equities i.e., stock price
Prev Close ==== Price stock closed in the last trading day    
Open       ==== Price stock open in the current trading day
High        ==== Highest  Price of stock  in the current trading day
Low                 ==== Lowest  Price of stock  in the current trading day
Last    Close   ==== Price stock close in the current trading day   
VWAP       ==== Volume weighted average price in the current trading day    
Volume      ==== Volume traded in the current trading day 
Turnover      ===  Total value traded in the current trading day. This is equal to VWAP X Volume
Trades     === Number of trades
Deliverable Volume === Number of shares which were bought . Excludes shares traded intra-day or bought and sold in the same day      
Deliverble  === Deliverable Volume/ Volume
Acknowledgements
Data sourced and scraped from NSE website

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43816) of an [OpenML dataset](https://www.openml.org/d/43816). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43816/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43816/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43816/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

